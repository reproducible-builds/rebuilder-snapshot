import argparse
import json
import sys

from reprobuilder import convert_flat_depends, count_flat_depends, merge_build_depends, convert_to_tuple_depends

def get_package_versions_combinations(tuple_depends, s_arch):
    if s_arch is None:
        return len(tuple_depends)
    pkgs = set([ (pkg, version, arch) for (pkg, version, arch) in tuple_depends.keys() if s_arch is None or s_arch == arch ])
    return len(pkgs)

def get_package_versions(tuple_depends, s_arch):
    pkgs = set([ (pkg, version) for (pkg, version, arch) in tuple_depends.keys() if s_arch is None or s_arch == arch ])
    return len(pkgs)

def get_packages(tuple_depends, s_arch):
    pkgs = set([ pkg for (pkg, version, arch) in tuple_depends.keys() if s_arch is None or s_arch  == arch ])
    return len(pkgs)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--builddepends',
                        help='Path to a build_depends.json when downloading additional files. Can be given multiple times.',
                        action='append',
                        default=[])
    parser.add_argument('--arch', help='architecture', action='append', default=[])
    parser.add_argument('--json', help='Output the result as json', action='store_true')
    args = parser.parse_args()

    archs = None
    if args.arch:
        archs = args.arch

    if not args.builddepends:
        args.builddepends = ['build_depends.json']

    bps = []
    for bp_file in args.builddepends:
        bps.append(json.load(open(bp_file, 'r')))

    build_depends = merge_build_depends(bps)

    tuple_depends = convert_to_tuple_depends(build_depends)
    # a package combo counts all binary packages (every package, for every version, for ever archi)
    package_combos = 0
    # package versions ignores the architecture
    package_versions = 0
    # packages, ignoring the versions
    packages = 0

    # count all architectures
    if not args.arch:
        package_combos = get_package_versions_combinations(tuple_depends, None)
        package_versions = get_package_versions(tuple_depends, None)
        packages = get_packages(tuple_depends, None)
    else:
        for arch in args.arch:
            package_combos += get_package_versions_combinations(tuple_depends, arch)
            package_versions += get_package_versions(tuple_depends, arch)
            packages += get_packages(tuple_depends, arch)

    if args.json:
        print(json.dumps({ 'packages': packages, 'package_combination': package_combos, 'package_versions': package_versions }))
    else:
        print(f"Build_depends: {packages} packages resulting in {package_versions} different versions. {package_combos} package/version/arch combinations.")
