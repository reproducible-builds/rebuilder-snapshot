unsorted design notes
=====================

the big picture
---------------

1 snapshot-caching server:
- serves all build-depends for:
  - amd64, i386, arm64, armhf (+ arch:all)
  - experimental, unstable, testing, stable, (oldstable...)
  - main and non-free-firmware (not contrib nor non-free)

rebuilders per arch:
- experimental, unstable, testing (trixie), stable (bookworm)
- internal db (sqlite, postgres, json files, whatever)
- external: json file

each rebuilder: 4 times a day for each arch/suite combination
- download packages.gz
- download missing buildinfo files
- delete obsolete buildinfo files
- download missing build depends
- cleanup obsolete buildinfo files

each rebuilder: constantly:
- check for packages which havent been rebuild
- rebuild packages which havent been rebuild
--> put result in db


howto create a chroot for rebuilding a package based on a .buildinfo file
-------------------------------------------------------------------------
python3 example_download_all_build_depends.py	# will download .debs into /tmp/foo
SOURCE_DEBS=/tmp/foo	# directory with all .deb files mentioned in a .buildinfo file
TARGET=/tmp/test
cd /tmp/foo
sudo dpkg-deb --extract base-files_*.deb $TARGET
for i in *.deb ; do sudo dpkg-deb --extract $i $TARGET ; done
sudo mkdir -p $TARGET/var/cache/apt/archives
sudo cp *.deb /tmp/test/var/cache/apt/archives/
sudo chroot $TARGET sh -c "dpkg --install --force-depends /var/cache/apt/archives/*.deb"
sudo chroot $TARGET sh -c "dpkg --configure -a"

or

<josch> apt-ftparchive packages . > Packages
<josch> apt-ftparchive release . > Release
<h01ger> so "use apt-ftparchive to create a http/file repo for mmdebstrap and then tell mmdebstrap to either setup usrmerge or not. no need to tell mmdebstrap the suite, an empty string '' is enough"
<josch> mmdebstrap can work with a file:// or copy:// repo
<josch> oh there is one problem: sbuild needs apt in the chroot
<josch> but apt will not be part of the buildinfo
<h01ger> but if we install apt, it will? & will it matter or what else to do? we can just install with dpkg
<josch> it's not possible to work around that if you use the schroot backend with sbuild
<josch> if you are fine with adding more packages than in the buildinfo, sure
<josch> you can build with dpkg-buildpackage inside the same mmdebstrap invocation that built the chroot
<josch> you can use /dev/null as the "chroot directory" and everything will remain ephemeral
<h01ger> and how to get the build .debs out?
<josch> --customzize-hook=copy-out ...
<josch> but you probably only want to verify hashes, no?
<h01ger> yes
<h01ger> i dont want to run diffoscope here. i want this service to be rather "slick" (as can be), for many folks to setup.
<josch> then no need to copy anything out :)
<h01ger> i probably want to copy out a json with the hashes, no, what else, parsing output?
<h01ger> i might even want two hashes per .deb: sha1 and sha256, cause i think i'd prefer to design this with sha256 in mind
<josch> | h01ger: i looked at debootsnap again and maybe you can mis-use it for your purposes with minimal hacks after all because it creates a directory with the right debs (but you have that already) and then calls apt-ftparchive on them
<josch> so you'd just have to replace the part that creates this directory with doing nothing because you already have a directory containing the right debs


sub-modules:
------------

download_packages.gz $suite
-> support for several packages.gz per suite:
	unstable
	testing + testing-updates
	stable + stable-security + stable-updates
	later: oldstable also
	main and non-free-firmware, not contrib, not non-free
-> create list of package/version in $suite

download_buildinfo $arch $package $version
-> provide one .buildinfo file

download_build_deps_as_specified_in_buildinfo $buildinfo
-> downloads debs into one directory

create_chroot_from_buildinfo ($arch) $buildinfo
-> creates a chroot

download_sources $package version
-> downloads sources

build_source_in_chroot $chroot $source
-> builds

cleanup_debs_unused_by_all_buildinfo_files
-> to save diskspace


