#!/usr/bin/env python3

# thanks to Michał Šrajer in https://serverfault.com/questions/345665/how-to-parse-and-convert-ini-file-into-bash-array-variables
# ported to python3 by Holger Levsen
# to be obsolete soon, once the shell scripts are gone

import sys, configparser

config = configparser.ConfigParser()
config.read_file(sys.stdin)

for sec in config.sections():
    print("declare -A %s" % (sec))
    for key, val in config.items(sec):
        print('%s[%s]="%s"' % (sec, key, val))
