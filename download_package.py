#!/usr/bin/env python3
# download package to the storage

import binascii
import hashlib
import logging
import re
import gzip
import argparse
import pathlib
import json
import threading
import time
import queue

import requests
from urllib3.util import parse_url, Url
import reprobuilder
from reprobuilder import get_filename

import email.parser
from pathlib import Path
from dataclasses import asdict, dataclass, field, replace

LOG = logging.getLogger('rebuilderstorage')

import threading

class StoppableThread(threading.Thread):
    """Thread class with a stop() method. The thread itself has to check
    regularly for the stopped() condition."""
    # https://stackoverflow.com/questions/323972/is-there-any-way-to-kill-a-thread

    def __init__(self,  *args, **kwargs):
        super(StoppableThread, self).__init__(*args, **kwargs)
        self._stop_event = threading.Event()

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

def store_add_pkg(db_store, name, version, arch, filename, sha1, sha256):
    if name not in db_store:
        db_store[name] = {}
    if version not in db_store[name]:
        db_store[name][version] = {}
    # TODO: raise exception if already there?
    db_store[name][version][arch] = (filename, sha1, sha256)

def store_check_pkg(db_store, name, version, arch):
    """ check if a package is in the db_store """
    if name not in db_store:
        return False

    if version not in db_store[name]:
        return False

    if arch in db_store[name][version]:
        return arch

    if 'all' in db_store[name][version]:
        return 'all'

    return False

def store_package(storage_path: pathlib.Path, content: bytes, name: str, filename: str, sha1: str, sha256: str) -> dict:
    if len(sha256) != 64:
        raise RuntimeError("Invalid sha256 '%s'" % sha256)
    if len(sha1) != 40:
        raise RuntimeError("Invalid sha1 '%s'" % sha1)

    # file
    prefix = reprobuilder.get_debian_prefix(name)
    fd_file = storage_path / f'files/{prefix}/{name}'
    fd_file.mkdir(parents=True, exist_ok=True)
    fn_file = fd_file / filename
    fn_file.write_bytes(content)

    # hardlink sha1
    fd_sha1 = storage_path / f'sha1/{sha1[0:2]}/{sha1[2:4]}/'
    fd_sha1.mkdir(parents=True, exist_ok=True)
    fn_sha1 = fd_sha1 / sha1
    try:
        fn_sha1.hardlink_to(fn_file)
    except FileExistsError:
        pass

    # sha256
    fd_sha256 = storage_path / f'sha256/{sha256[0:2]}/{sha256[2:4]}/'
    fd_sha256.mkdir(parents=True, exist_ok=True)
    fn_sha256 = fd_sha256 / sha256
    try:
        fn_sha256.hardlink_to(fn_file)
    except FileExistsError:
        pass

def store_package_storepackage(storage_path: pathlib.Path, storepackage: reprobuilder.StorePackage, content: bytes) -> dict:
    """ Stores a package to the file storage. The package has also the meta data as storepackage """
    filename = get_filename(storepackage.name, storepackage.version, storepackage.arch)
    store_package(storage_path, content, storepackage.name, filename, storepackage.sha1, storepackage.sha256)

    return {'action': 'add', 'package': storepackage.name, 'version': storepackage.version, 'arch': storepackage.arch, 'filename': filename, 'sha1': storepackage.sha1, 'sha256': storepackage.sha256}

def store_package_binpackage(storage_path: pathlib.Path, binpackage: reprobuilder.BinPackage, content: bytes, sha1: str) -> dict:
    """ Stores a package to the file storage. The package has also the meta data as binpackage """
    if len(binpackage.sha256) != 64:
        raise RuntimeError("Invalid sha256 '%s' for package: %s" % (binpackage.sha256, binpackage.name))

    filename = get_filename(binpackage.name, binpackage.version, binpackage.arch)
    store_package(storage_path, content, binpackage.name, filename, sha1, binpackage.sha256)

    return {'action': 'add', 'package': binpackage.name, 'version': binpackage.version, 'arch': binpackage.arch, 'filename': filename, 'sha1': sha1, 'sha256': binpackage.sha256}

def get_pool_url(package_url: str) -> str:
    """ Return the root of the pool url to download package. Highly inefficient."""
    purl = parse_url(package_url)
    path = purl.path
    # ('/', 'debian', 'dists', 'bullseye', 'non-free', 'binary-amd64', 'Packages.gz')
    parts = list(pathlib.Path(path).parts)
    # drop /
    del parts[0]
    # drop Packages.gz
    parts.pop()
    # drop binary-amd64
    parts.pop()
    # drop non-free
    parts.pop()
    # drop bullseye
    parts.pop()
    # drop dists
    parts.pop()

    new_path = '/' + '/'.join(parts)
    new_url = Url(purl.scheme, purl.auth, purl.host, purl.port, new_path)
    return new_url.url

def download_buildd(name: str, version: str, arch: str, buildd_urls: list, session: requests.Session):
    """ Download a package from another buildd server """
    if not buildd_urls:
        return None

    if session is None:
        session = requests.Session()

    for url in buildd_urls:
        package_url = f'{url}/api/v1/package2/{name}/{version}/{arch}'
        try:
            resp = session.get(package_url)
            if resp.status_code == 404:
                continue
            resp.raise_for_status()
            package = resp.json()
            storepackage = reprobuilder.StorePackage(**package)
            if name != storepackage.name:
                continue
            if version != storepackage.version:
                continue
            # ensure the filename is correct
            storepackage.filename = get_filename(storepackage.name, storepackage.version, storepackage.arch)

            LOG.debug(f'Found package {name}/{version}/{storepackage.arch} in buildd {url}. Trying to download')
            resp = session.get(f'{url}/api/v1/download/{name}/{version}/{storepackage.arch}')
            resp.raise_for_status()
            LOG.debug(f'Downloaded package {name}/{version}/{arch} from buildd.')
            return (resp.content, storepackage)
        except:
            LOG.exception(f'Failed to download: {name}/{version}/{arch} from buildd {url}.')
            continue

    return None

def find_download_repo(name, version, arch, repo_cache, session: requests.Session):
    """ Download a package from the repository """
    if session is None:
        session = requests.Session()

    for url, repo in repo_cache.items():
        if name not in repo:
            continue

        pkg = repo[name]
        if version != pkg.version:
            continue

        LOG.debug('Found package %s/%s/%s in repository. Trying to download' % (name, version, arch))
        pool_url = get_pool_url(url)
        try:
            resp = session.get(f'{pool_url}/{pkg.filename}')
            resp.raise_for_status()
            LOG.debug('Downloaded package %s/%s/%s from repository.' % (name, version, arch))
            return (resp.content, pkg)
        except:
            LOG.exception(f'Failed to download: {pool_url}/{pkg.filename}')
            continue

    return None

def download_single_file(storage_path, package, version, arch, repo_cache, session, buildd_urls) -> tuple:
    # repo
    try:
        result = find_download_repo(package, version, arch, repo_cache, session)
        if result is not None:
            content, binpackage = result
            sha256 = str(binascii.b2a_hex(hashlib.sha256(content).digest()), 'utf-8')
            if sha256 != binpackage.sha256:
                raise RuntimeError("Repo downloaded sha256 wrong")
            sha1 = str(binascii.b2a_hex(hashlib.sha1(content).digest()), 'utf-8')
            log_entry = store_package_binpackage(storage_path, binpackage, content, sha1)
            LOG.info('Stored package %s/%s/%s using repos' % (binpackage.name, binpackage.version, binpackage.arch))
            return (True, log_entry)
    except Exception as exp:
        # TODO: log exception with level debug using format exception
        LOG.exception('%s/%s repo exception' % (package, version))

    # try another buildd
    try:
        result = download_buildd(package, version, arch, buildd_urls, session)
        if result is not None:
            content, storepackage = result
            sha1 = str(binascii.b2a_hex(hashlib.sha1(content).digest()), 'utf-8')
            if sha1 != storepackage.sha1:
                raise RuntimeError("Repo downloaded sha1 wrong")
            sha256 = str(binascii.b2a_hex(hashlib.sha256(content).digest()), 'utf-8')
            if sha256 != storepackage.sha256:
                raise RuntimeError("Repo downloaded sha256 wrong")
            log_entry = store_package_storepackage(storage_path, storepackage, content)
            LOG.info('Stored package %s/%s/%s using buildd' % (storepackage.name, storepackage.version, storepackage.arch))
            return (True, log_entry)
    except Exception as exp:
        # TODO: log exception with level debug using format exception
        LOG.exception('%s/%s buildd exception' % (package, version))

    # snapshot
    try:
        result, pkgobj = reprobuilder.download_snapshot_single(package, version, arch, session)
        if result:
            package, s_arch, sha1, content = pkgobj
            computed_sha1 = str(binascii.b2a_hex(hashlib.sha1(content).digest()), 'utf-8')
            if sha1 != computed_sha1:
                raise RuntimeError("Snapshot downloaded sha1 wrong")

            sha256 = str(binascii.b2a_hex(hashlib.sha256(content).digest()), 'utf-8')
            filename = get_filename(package, version, s_arch)
            store_package(storage_path, content, package, filename, sha1, sha256)
            log_entry = {'action': 'add', 'package': package, 'version': version, 'arch': s_arch, 'filename': filename, 'sha1': sha1, 'sha256': sha256}
            LOG.info('Stored package %s/%s/%s using snapshot' % (package, version, s_arch))
            return (True, log_entry)
        else:
            _, exp = pkgobj
            LOG.debug('%s/%s/%s Snapshot failed to download with %s' % (package, version, arch, exp))
    except Exception as exp:
        LOG.exception('%s/%s/%s Snapshot exception' % (package, version, arch))

    return (False, (package, version, arch))

def download_file_worker(tasks: queue.Queue, results: queue.Queue, arch: str, repo_cache: dict, storage_path: pathlib.Path, buildd_urls: list):
    thread = threading.current_thread()
    session = requests.Session()
    while not thread.stopped():
        task = tasks.get()
        if task is None:
            tasks.task_done()
            return

        name, version = task
        result = download_single_file(storage_path, name, version, arch, repo_cache, session, buildd_urls)
        tasks.task_done()
        results.put(result)

    LOG.info('Finished worker')

def download_files_threaded(*, buildd_urls: list = None, packages: list = None, arch: str = None, packages_urls: list = None, storage_path: pathlib.Path = None, db_store: dict = None, log_store: list = None, num_threads: int = 4) -> tuple:
    session = requests.Session()

    if storage_path is None:
        raise RuntimeError("No storage_path given")

    if db_store is None:
        db_store = {}

    if log_store is None:
        log_store = []

    repo_cache = {}
    for url in packages_urls:
        try:
            repo = reprobuilder.download_packages(url)
        except:
            LOG.info("Failed to download %s. Ignoring package repo." % url)
            continue
        repo_cache[url] = repo

    # count all deps
    overall = 0
    for _, versions in packages.items():
        overall += len(versions)
    LOG.info(f"Found {overall} build dependencies to download")

    results = queue.Queue()
    tasks = queue.Queue()
    threads = []
    for i in range(num_threads):
        thread = StoppableThread(target=download_file_worker, args=[tasks, results, arch, repo_cache, storage_path, buildd_urls])
        threads.append(thread)
        thread.start()

    for package, versions in packages.items():
        for version in versions:
            # check if already present
            db_arch = store_check_pkg(db_store, package, version, arch)
            if not db_arch:
                db_arch = store_check_pkg(db_store, package, version, "all")
            if db_arch:
                LOG.info("Package already in store %s/%s/%s" % (package, version, db_arch))
                continue
            try:
                tasks.put((package, version))
            except KeyboardInterrupt as exp:
                for thread in threads:
                    thread.stop()
                LOG.warning('Aborting as user requests')
                break

    LOG.info('Finished queueing tasks')
    # to stop workers
    for i in threads:
        tasks.put(None)

    def parse_results(resultpair):
        result, obj = resultpair
        if result:
            log_entry = obj
            log_store.append(log_entry)
            store_add_pkg(
                    db_store,
                    log_entry['package'],
                    log_entry['version'],
                    log_entry['arch'],
                    log_entry['filename'],
                    log_entry['sha1'],
                    log_entry['sha256'])
        # TODO: failed case

    LOG.info("Waiting for workers to clear the tasks: %d" % tasks.qsize())
    while not tasks.empty():
        LOG.info("Waiting for workers to clear the tasks: %d" % tasks.qsize())
        try:
            while not results.empty():
                try:
                    resultpair = results.get_nowait()
                    parse_results(resultpair)
                except queue.Empty:
                    pass
            time.sleep(1)
            LOG.info('Progress %d/%d (remain/all)' % (tasks.qsize(), overall))
        except KeyboardInterrupt:
            for thread in threads:
                thread.stop()
            LOG.warning('Aborting as user requests')
            break

    tries = 3
    # TODO: check for tasks.join? but how to work with user abort?
    while True:
        try:
            result = results.get(timeout=1)
        except queue.Empty:
            if tries <= 0:
                break
            tries -= 1
            continue

        parse_results(result)

    LOG.info('Progress %d/%d (remain/all)' % (tasks.qsize(), overall))
    return db_store, log_store

def download_files(packages: list = None, arch: str = None, packages_urls: list = None, storage_path: pathlib.Path = None, db_store: dict = None, log_store: list = None) -> tuple:
    session = requests.Session()

    if storage_path is None:
        raise RuntimeError("No storage_path given")

    if db_store is None:
        db_store = {}

    if log_store is None:
        log_store = []

    repo_cache = {}
    for url in packages_urls:
        try:
            repo = reprobuilder.download_packages(url)
        except:
            LOG.info("Failed to download %s. Ignoring package repo." % url)
            continue
        repo_cache[url] = repo

    # count all deps
    overall = 0
    for _, versions in packages.items():
        overall += len(versions)
    LOG.info(f"Found {overall} build dependencies to download")

    for package, versions in packages.items():
        for version in versions:
            try:
                # check if already present
                db_arch = store_check_pkg(db_store, package, version, arch)
                if db_arch:
                    LOG.info("Package already in store %s/%s/%s" % (package, version, db_arch))
                    continue
                # repo
                try:
                    result = find_download_repo(package, version, arch, repo_cache, session)
                    if result is not None:
                        content, binpackage = result
                        sha256 = str(binascii.b2a_hex(hashlib.sha256(content).digest()), 'utf-8')
                        if sha256 != binpackage.sha256:
                            raise RuntimeError("Repo downloaded sha256 wrong")

                        sha1 = str(binascii.b2a_hex(hashlib.sha1(content).digest()), 'utf-8')
                        log_entry = store_package_binpackage(storage_path, binpackage, content, sha1)
                        log_store.append(log_entry)
                        store_add_pkg(db_store, binpackage.name, binpackage.version, binpackage.arch, log_entry.filename, sha1, sha256)
                        LOG.info('Stored package %s/%s/%s using repos' % (binpackage.name, binpackage.version, binpackage.arch))
                        continue
                except KeyboardInterrupt:
                    # TODO: kill
                    LOG.error("User aborted")
                    return db_store, log_store
                except Exception as exp:
                    # TODO: log exception with level debug using format exception
                    LOG.exception('%s/%s repo exception' % (package, version))

                # snapshot
                try:
                    result, pkgobj = reprobuilder.download_snapshot_single(package, version, arch, session)
                    if result:
                        package, s_arch, sha1, content = pkgobj
                        computed_sha1 = str(binascii.b2a_hex(hashlib.sha1(content).digest()), 'utf-8')
                        if sha1 != computed_sha1:
                            raise RuntimeError("Snapshot downloaded sha1 wrong")

                        sha256 = str(binascii.b2a_hex(hashlib.sha256(content).digest()), 'utf-8')
                        filename = get_filename(package, version, s_arch)
                        store_package(storage_path, content, package, filename, sha1, sha256)
                        log_entry = {'action': 'add', 'package': package, 'version': version, 'arch': s_arch, 'filename': filename, 'sha1': sha1, 'sha256': sha256}
                        log_store.append(log_entry)
                        store_add_pkg(db_store, package, version, s_arch, filename, sha1, sha256)
                        LOG.info('Stored package %s/%s/%s using snapshot' % (package, version, s_arch))
                        continue
                    else:
                        _, exp = pkgobj
                        LOG.debug('%s/%s/%s Snapshot failed to download with %s' % (package, version, arch, exp))
                except KeyboardInterrupt:
                    # TODO: kill
                    LOG.error("User aborted")
                    return db_store, log_store
                except Exception as exp:
                    LOG.exception('%s/%s/%s Snapshot exception' % (package, version, arch))

                # TODO: add to failed
                LOG.error("Failed to find a package for %s/%s/%s" % (package, version, arch))
            except KeyboardInterrupt:
                # TODO: kill
                LOG.error("User aborted")
                return db_store, log_store
    return db_store, log_store

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--packages-url', help='an URL to a Packages or Packages.gz or Packages.xz. Can be used multiple times.', action='append', default=[])
    parser.add_argument('--cache-url', help='an URL to a snapshot-buildd server. Can be used multiple times.', action='append', default=[])
    parser.add_argument('--builddepends',
                        help='Path to a build_depends.json when downloading additional files. Can be given multiple times.',
                        action='append',
                        default=[])
    parser.add_argument('--storage', help='Path to the root of the filesystem store', default='./storage/')
    parser.add_argument('--database', help='Path to the storage.json', default='storage.json')
    parser.add_argument('--logbase', help='Path to the log.json', default='log.json')
    parser.add_argument('--errors', help='Path to save failed packages', default='download_packages.failed.json')
    parser.add_argument('--retry', help='Retry only failed packages. Errors file must exist', action='store_true')
    parser.add_argument('--arch', help='architecture', required=True)
    parser.add_argument('--threads', help='threads', default=4, type=int)
    parser.add_argument('--verbose', help='enable verbose output', action='store_true')
    parser.add_argument('--debug', help='enable debug output', action='store_true')
    args = parser.parse_args()

    # logging
    format_line = '%(asctime)s: %(name)s %(levelname)s - %(message)s'
    log_level = logging.WARNING
    if args.debug:
        log_level=logging.DEBUG
    elif args.verbose:
        log_level=logging.INFO
    logging.basicConfig(
            level=log_level,
            format=format_line
    )

    filelogger = logging.FileHandler(filename='download.log')
    filelogger.setLevel(log_level)
    filelogger.setFormatter(logging.Formatter(format_line))
    logging.root.addHandler(filelogger)


    errors = pathlib.Path(args.errors)
    storage = pathlib.Path(args.storage)
    database = pathlib.Path(args.database)
    logbase = pathlib.Path(args.logbase)

    if not args.builddepends:
        args.builddepends = ['build_depends.json']

    bps = []
    for bp_file in args.builddepends:
        builddepends = pathlib.Path(bp_file)
        if not builddepends.exists():
            raise RuntimeError("Builddepends '%s' must exist!" % builddepends)
        bps.append(json.load(builddepends.open('r')))
    builddepends_store = reprobuilder.merge_build_depends(bps)

    if not storage.exists():
        LOG.info("Storage doesn't exist. Creating directory %s" % storage)
        storage.mkdir()
    elif not storage.is_dir():
        raise RuntimeError("Storage '%s' must be a directory!" % storage)

    db_store = {}
    if database.exists():
        db_store = json.load(database.open('r'))

    log_store = []
    if logbase.exists():
        log_store = json.load(logbase.open('r'))

    flat_build_depends = reprobuilder.convert_flat_depends(builddepends_store, [args.arch, "all"])
    db_store, log_store = download_files_threaded(
            arch=args.arch,
            packages=flat_build_depends, packages_urls=args.packages_url,
            storage_path=storage, db_store=db_store, log_store=log_store,
            buildd_urls=args.cache_url,
            num_threads=args.threads)

    json.dump(db_store, database.open('w'))
    json.dump(log_store, logbase.open('w'))

    # TODO: check build_depends by json schema
    # TODO: check storage.json


# TODO: select a specific package or all
# optional: download packages.gz
# optional: optional: cache downloaded packages.gz

# download + validate (sha256) from public repo
# download + validate (sha1) from snapshot
# generate sha1/sha256
# copy package to dl/file/0/0d_foo_bar.deb
# hardlink to dl/sha1/00/00/full_hash.deb
# hardlink to dl/sha256/00/00/full_hash.deb
# write a json-file of all done operations
# write storage.json
