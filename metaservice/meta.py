from flask import Flask, render_template, request, url_for, flash, redirect, abort, g

import pathlib
from dataclasses import asdict, fields
from reprobuilder import StorePackage, get_debian_prefix
import sqlite3

app = Flask(__name__)

# FIXME: use config
DATABASE = './storage.sqlite'
WEBROOT = '/rebuilder-snapshot'

def data_fields(dataclass):
    return (field.name for field in fields(dataclass))

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

@app.route("/")
def about():
    return """
    <p>rebuilder-snapshot meta service</p>
    <br/>
    <p>Providing meta data to this snapshot-rebuilder service.</p>
    <ul>
     <li> /v1/path/{fullpath} - returns the meta information assigned to the path</li>
     <li> /v1/filename/{filename} - returns the meta information assigned to the short filename</li>
     <li> /v1/sha1/{sha1} - returns the meta information assigned to the sha1</li>
     <li> /v1/sha256/{sha256} - returns the meta information assigned to the sha1</li>
     <li> /v1/package/{name} - all available versions</li>
     <li> /v1/package/{name}/{version} - all available architectures</li>
     <li> /v1/package/{name}/{version}/{arch} - returns the package with given arch</li>
     <li> /v1/package2/{name}/{version}/{arch} - returns the package in given arch or all</li>
     <li> /v1/download/{name}/{version}/{arch} - redirect to the package or 404</li>
     <li> /v1/download2/{name}/{version}/{arch} - redirect to the package or 404 (if matches arch or arch == all)</li>
     <li> /v1/version - returns { 'version': 1 }</li>
     <li> /version - returns { 'supported_versions': [1] }</li>
    <ul>
    """

@app.route('/v1/filename/<filename>')
def get_filename(filename):
    cur = get_db().cursor()
    package = cur.execute('SELECT %s from storage where filename = ?' % ", ".join(data_fields(StorePackage)), [filename]).fetchone()
    if package is None:
        abort(404)

    package = StorePackage(*package)
    return asdict(package)

@app.route('/v1/fullpath/<path:subpath>')
def get_fullpath(subpath):
    give_path = pathlib.Path(subpath)
    filename = give_path.parts[-1]
    prefix = get_debian_prefix(filename)
    calc_path = pathlib('files/') / prefix / filename
    if calc == give_path:
        return get_filename(path.parts[-1])
    abort(404)

@app.route('/v1/sha256/<sha256>')
def get_sha256(sha256):
    cur = get_db().cursor()
    package = cur.execute('SELECT %s from storage where sha256 = ?' % ", ".join(data_fields(StorePackage)), [sha256]).fetchone()
    if package is None:
        abort(404)

    package = StorePackage(*package)
    return asdict(package)

@app.route('/v1/sha1/<sha1>')
def get_sha1(sha1):
    cur = get_db().cursor()
    package = cur.execute('SELECT %s from storage where sha1 = ?' % ", ".join(data_fields(StorePackage)), [sha1]).fetchone()
    if package is None:
        abort(404)

    package = StorePackage(*package)
    return asdict(package)

@app.route('/v1/package/<name>')
def get_package(name):
    cur = get_db().cursor()
    versions = cur.execute('SELECT version from storage where name = ?', [name]).fetchall()
    if not versions:
        abort(404)

    # unpack tuple
    return [x[0] for x in versions]

@app.route('/v1/package/<name>/<version>')
def get_package_version(name, version):
    cur = get_db().cursor()
    archs = cur.execute('SELECT arch from storage where name = ? and version = ?', [name, version]).fetchall()
    if not archs:
        abort(404)

    # unpack tuple
    return [x[0] for x in archs]

@app.route('/v1/package/<name>/<version>/<arch>')
def get_package_version_arch(name, version, arch):
    cur = get_db().cursor()
    package = cur.execute('SELECT %s from storage where name = ? and version = ? and arch = ?' % ", ".join(data_fields(StorePackage)),
            [name, version, arch]).fetchone()
    if not package:
        abort(404)

    package = StorePackage(*package)
    return asdict(package)

@app.route('/v1/package2/<name>/<version>/<arch>')
def get_package_version_arch_or_all(name, version, arch):
    cur = get_db().cursor()
    package = cur.execute('SELECT %s from storage where name = ? and version = ? and arch = ?' % ", ".join(data_fields(StorePackage)),
            [name, version, arch]).fetchone()
    if not package:
        package = cur.execute('SELECT %s from storage where name = ? and version = ? and arch = ?' % ", ".join(data_fields(StorePackage)),
                [name, version, "all"]).fetchone()
    if not package:
        abort(404)

    package = StorePackage(*package)
    return asdict(package)

@app.route('/v1/download/<name>/<version>/<arch>')
def get_download_version_arch_or_all(name, version, arch):
    cur = get_db().cursor()
    filename = cur.execute('SELECT filename from storage where name = ? and version = ? and arch = ?',
            [name, version, arch]).fetchone()
    if not filename:
        abort(404)

    filename = filename[0]

    prefix = get_debian_prefix(filename)
    path = pathlib.Path(WEBROOT) / 'storage' / 'files' / prefix / name / filename
    return redirect(path, 307)

@app.route('/v1/download2/<name>/<version>/<arch>')
def get_download2_version_arch_or_all(name, version, arch):
    cur = get_db().cursor()
    filename = cur.execute('SELECT filename from storage where name = ? and version = ? and arch = ?',
            [name, version, arch]).fetchone()
    if not filename:
        filename = cur.execute('SELECT filename from storage where name = ? and version = ? and arch = ?',
                [name, version, "all"]).fetchone()
    if not filename:
        abort(404)

    filename = filename[0]

    prefix = get_debian_prefix(filename)
    path = pathlib.Path(WEBROOT) / 'storage' / 'files' / prefix / name / filename
    return redirect(path, 307)

@app.route('/v1/version')
def get_version():
    response = {'version': 'v1'}
    return response

@app.route('/version')
def get_global_version():
    response = {'supported_versions': [1]}
    return response

if __name__ == "__main__":
    app.run(host='127.0.0.1')
