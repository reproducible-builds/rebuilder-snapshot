# meta service for snapshot

## How to Install

```
cd metaservice/

cp ../storage.sqlite .
ln -s ../reprobuilder.py
vim meta.py
# edit WEBROOT and DATABASE
```

## How to run it (as development)

```
cd metaservice/
flask --app meta run
```

## TODOs

* use flask config
* remove the symlink by creating a real module
