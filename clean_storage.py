#!/usr/bin/env python3

import logging
import argparse
from reprobuilder import convert_flat_depends
from pathlib import Path
import json

def convert_flat_to_package(flat_package: list) -> dict:
    # convert flat packages a list(package, version, arch, filename, sha1, sha256) into a dict of
    # { 'pkgname': { 'version': { 'arch': (filename, sha1, sha256) } } }

    packages = {}

    for pkg in flat_package:
        name, version, arch, filename, sha1, sha256 = pkg
        if name not in packages:
            packages[name] = {}
        if version not in packages[name]:
            packages[name][version] = {}

        packages[name][version][arch] = [filename, sha1, sha256]
    return packages

def clean_storage(*, arch: str, packages: dict,
            storage_path: Path, db_store: dict,
            log_store: list, dry: bool):

    # go over all packages
    # check if packages are part of drop

    def in_build_depends(package, version):
        if package not in packages:
            return False
        return version in packages[package]

    drop_packages = [(package, version, arch, filename, sha1, sha256)
            for package, versions in db_store.items()
                for version, archs in versions.items()
                    for arch, (filename, sha1, sha256) in archs.items() if not in_build_depends(package, version)]
    # convert flat packages a list(package, version, arch, filename, sha1, sha256) into a dict of
    # { 'pkgname': { 'version': { 'arch': (filename, sha1, sha256) } } }
    drop_packages = convert_flat_to_package(drop_packages)

    return db_store, log_store, drop_packages

TEST_DB_STORE = {
        "autoconf": {
            "2.69-11.1": {
                "all": [
                    "autoconf_2.69-11.1_amd64.deb",
                "450003daac056edb92cef46da8c5ab88dc349b50",
                "725bf393fcbb6e3b8c309b10ffd19be72ad697ea749e2b85d9addd5349f8b310"
                ]
            },
            "2.71-2": {
                "all": [
                    "autoconf_2.71-2_amd64.deb",
                "e3b5479eac61532f93bb5cead13dc87a4ab55aac",
                "5ce882c781012599767b9fe9a10da39e14b45ea90a01a4ed96e5392eb2df99c0"
                ]
            }
        },
        "automake": {
            "1:1.16.1-3": {
                "all": [
                    "automake_1:1.16.1-3_amd64.deb",
                "054b4186e10bf786d584de709f956bd5e0c3f5b5",
                "eef02b7b0efe7f7900cd3b953cd32d631f7d42f8c0080db3c7f53bf3e38adb30"
                ]
            },
            "1:1.15-6": {
                "all": [
                    "automake_1:1.15-6_amd64.deb",
                "1f93e0524b32e3e8890bc0fd47d0f3e37f46e5db",
                "3ae460e116f1d6e5b8f76bdbc2dd52b9267484efeb80063c8a163c076d2ba108"
                ]
            },
        }
    }
def _test_base(packages: dict = None, db_store: dict = None, expect_drop_packages: dict = None):

    log_store = []
    if db_store is None:
        db_store = TEST_DB_STORE

    if packages is None:
        packages = {}

    if expect_drop_packages is None:
        expect_drop_packages = {}

    rdb_store, rlog_store, drop_packages = clean_storage(arch='amd64', packages=packages,
            storage_path='/tmp/storage', db_store=db_store,
            log_store=log_store, dry=True)

    assert(drop_packages == expect_drop_packages)

def test_drop_package_empty_build_depends():
    expect_drop_packages = TEST_DB_STORE
    _test_base(packages = {}, expect_drop_packages = expect_drop_packages)

def test_drop_package_build_depends1():
    # results: one entry in the list is a {package: set(versiona, versionb)}
    packages = {
        'automake': set(['1:1.16.1-3']),
        'autoconf': set(['2.69-11.1']),
    }

    expect_drop_packages = {
        "autoconf": {
            "2.71-2": {
                "all": [
                    "autoconf_2.71-2_amd64.deb",
                    "e3b5479eac61532f93bb5cead13dc87a4ab55aac",
                    "5ce882c781012599767b9fe9a10da39e14b45ea90a01a4ed96e5392eb2df99c0"
                ]
            }
        },
        "automake": {
            "1:1.15-6": {
                "all": [
                    "automake_1:1.15-6_amd64.deb",
                    "1f93e0524b32e3e8890bc0fd47d0f3e37f46e5db",
                    "3ae460e116f1d6e5b8f76bdbc2dd52b9267484efeb80063c8a163c076d2ba108"
                ]
            },
        }
    }
    _test_base(packages = packages, expect_drop_packages = expect_drop_packages)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description="Remove old/unused packages from the storage.\n"
                        "Take a build_depends.json and storage.json as input.\n"
                        "Check if storage.json contains packages which aren\'t references in the build_depends.json")
    parser.add_argument('--builddepends', help='Path to a build_depends.json when downloading additional files', default='build_depends.json')
    parser.add_argument('--storage', help='Path to the root of the filesystem store', default='./storage/')
    parser.add_argument('--database', help='Path to the storage.json', default='storage.json')
    parser.add_argument('--drop', help='Path to the storage.json', default='drop.json')
    parser.add_argument('--logbase', help='Path to the log.json', default='log.json')
    parser.add_argument('--errors', help='save failed packages', default='download_packages.failed.json')
    parser.add_argument('--retry', help='Retry only failed packages. Errors file must exist', action='store_true')
    parser.add_argument('--arch', help='architecture', required=True)
    # parser.add_argument('--threads', help='threads', default=4, type=int)
    parser.add_argument('--verbose', help='enable verbose output', action='store_true')
    parser.add_argument('--debug', help='enable debug output', action='store_true')
    parser.add_argument('--dry', help='only check for old packages and output them into dry.json, but don\'t do anything', action='store_true')
    parser.add_argument('--remove', help='Remove packages which are not part of build_depends anymore.', action='store_true')
    args = parser.parse_args()

    if args.remove and args.dry:
        raise RuntimeError("Can't use --remove and --dry at the same time!")

    if args.remove:
        raise NotImplemented('--remove not yet implemented. Please open a PR.')

    # logging
    format_line = '%(asctime)s: %(name)s %(levelname)s - %(message)s'
    log_level = logging.WARNING
    if args.debug:
        log_level=logging.DEBUG
    elif args.verbose:
        log_level=logging.INFO
    logging.basicConfig(
            level=log_level,
            format=format_line
    )

    filelogger = logging.FileHandler(filename='clean_storage.log')
    filelogger.setLevel(log_level)
    filelogger.setFormatter(logging.Formatter(format_line))
    logging.root.addHandler(filelogger)

    builddepends_store = None
    builddepends = Path(args.builddepends)
    drop = Path(args.drop)
    errors = Path(args.errors)
    storage = Path(args.storage)
    database = Path(args.database)
    logbase = Path(args.logbase)

    if not builddepends.exists():
        raise RuntimeError("Builddepends '%s' must exist!" % builddepends)
    else:
        builddepends_store = json.load(builddepends.open('r'))

    if not storage.exists() and not args.dry:
        raise RuntimeError("Storage '%s' must exist!" % builddepends)
    elif not storage.is_dir():
        raise RuntimeError("Storage '%s' must be a directory!" % storage)

    if not database.exists():
        raise RuntimeError("storage.json under '%s' must exist!" % database)
    db_store = json.load(database.open('r'))

    log_store = []
    if logbase.exists():
        log_store = json.load(logbase.open('r'))

    flat_build_depends = convert_flat_depends(builddepends_store, ['all', 'amd64'])

    db_store, log_store, drop_packages = clean_storage(
            arch=args.arch, packages=flat_build_depends,
            storage_path=storage, db_store=db_store,
            log_store=log_store, dry=args.dry)

    json.dump(drop_packages, drop.open('w'))

    if not args.dry:
        json.dump(db_store, database.open('w'))
        json.dump(log_store, logbase.open('w'))
