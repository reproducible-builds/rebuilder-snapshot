#!/usr/bin/env python

import argparse
import json
import logging
from pathlib import Path
import sqlite3

LOG = logging.getLogger("storage_to_sqlite")

def convert_to_sqlite(db_store: dict, sqlite: Path):
    LOG.info("Creating sqlite3 under {db_store}")
    db = sqlite3.connect(sqlite)
    with open('./database/schema.sqlite.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()

    for_sqlite = [(package, version, arch, filename, sha1, sha256)
            for package, versions in db_store.items()
                for version, archs in versions.items()
                    for arch, (filename, sha1, sha256) in archs.items()]

    LOG.info("Found {len(for_sqlite)} entries in storage. Inserting into database.")
    cur = db.cursor()
    cur.executemany("INSERT INTO storage(name, version, arch, filename, sha1, sha256) VALUES(?, ?, ?, ?, ?, ?) ", for_sqlite)
    db.commit()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description="Convert the storage.json into an sqlite database")
    parser.add_argument('--database', help='Path to the storage.json', default='storage.json')
    parser.add_argument('--sqlite', help='Path to the sqlite', default='storage.sqlite')
    parser.add_argument('--verbose', help='enable verbose output', action='store_true')
    parser.add_argument('--debug', help='enable debug output', action='store_true')
    args = parser.parse_args()

    # logging
    format_line = '%(asctime)s: %(name)s %(levelname)s - %(message)s'
    log_level = logging.WARNING
    if args.debug:
        log_level=logging.DEBUG
    elif args.verbose:
        log_level=logging.INFO
    logging.basicConfig(
            level=log_level,
            format=format_line
    )

    filelogger = logging.FileHandler(filename='storage_to_sqlite.log')
    filelogger.setLevel(log_level)
    filelogger.setFormatter(logging.Formatter(format_line))
    logging.root.addHandler(filelogger)

    database = Path(args.database)
    sqlite = Path(args.sqlite)

    if not database.exists():
        raise RuntimeError("storage.json under '%s' must exist!" % database)
    db_store = json.load(database.open('r'))

    if sqlite.exists():
        raise RuntimeError("sqlite.db under '%s' already exist!" % sqlite)

    convert_to_sqlite(db_store, sqlite)
