setup notes
-----------
rebuilder-snapshot is split into several parts:
- the incomplete snapshot mirror serving all build-depends
  - TODO: add location rewrite /rebuilder-snapshot/ to nginx.conf
- DONE: data export for tracker.d.o/britney etc
  - thats metaservice/README.md
  - edit metaservice/snapshot-buildd-meta.service
  - metaservice/meta.py has "FIXME: use config" and needed editing
- TODO: debian rebuilder using this
- TODO: visualisation of the rebuilder results.

alpha setup for rebuilder-snapshot.debian.net
---------------------------------------------
- decide where to install, where the data should live
  - osuosl5: SSDs, 64g RAM, 16 cores, no other usages, wheehooo
- under which user to run
	jenkins
- where: 
	data: /srv/data/rebuilder-snapshot owned by jenkins
	code: /srv/rebuilder-snapshot owned by jenkins-adm
- todo for rebuilder-snapshot.sh
  - fix FIXME
  - --output ${arch}_build_depends.json ?_
  - run this script via a simple systemd service or timer, which only runs if configured
- instructions to serve the files via a httpd server
- package this? maybe. the rebuilder for sure.

misc
----
- rename get_all_build_depends.py to gather_build_depends_information.py ?
- rename download_package.py to download_all_build_depends.py ?
