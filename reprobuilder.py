#!/usr/bin/env python3

import logging
import re
import gzip
import lzma

import requests

import email.parser
from pathlib import Path
from dataclasses import asdict, dataclass, field

LOG = logging.getLogger('reprodeb')

SNAPSHOT_HOST = 'snapshot-cloudflare.debian.org'

@dataclass
class StorePackage:
    name: str
    version: str
    arch: str
    sha1: str = ''
    sha256: str = ''
    # make_1.2_amd64.deb
    filename: str = ''

@dataclass
class BinPackage:
    name: str
    version: str
    source: str
    source_version: str
    arch: str
    sha256: str = ''
    filename: str = ''

def get_source_package(deb_file: str) -> tuple:
    """ get a binary .deb file to get the source package name """
    source_package = "bash"
    package = "bash-doc"
    arch = "all"
    version = "5.2.15-2"
    return (source_package, package, arch, version)

EPOCH_REX = re.compile(r'^((?P<epoch>[0-9]+):){1}(?P<version>.+)')
def get_build_info(source_package: str, version: str, arch: str, session: requests.Session) -> bytes:
    if session is None:
        session = requests.Session()

    prefix = source_package[0]
    if source_package.startswith('lib'):
        prefix = source_package[0:4]

    # drop the epoch in '4:22.0.3' -> '22.0.3'
    match = EPOCH_REX.match(version)
    if match:
        version = match.groupdict()['version']

    url = f"https://buildinfos.debian.net/buildinfo-pool/{prefix}/{source_package}/{source_package}_{version}_{arch}.buildinfo"
    resp = session.get(url)
    resp.raise_for_status()
    return str(resp.content, 'utf-8')

def parse_package_file(packages_str: str) -> dict:
    packages = {}
    linebuffer = ""
    skip_lines = 0
    for line in packages_str.splitlines():
        if line.startswith('-----BEGIN PGP SIGNED MESSAGE-----'):
            skip_lines = 3
        if skip_lines > 0:
            skip_lines -= 1

        if line == "":
            parser = email.parser.Parser()
            package = parser.parsestr(linebuffer)
            packages[package["Package"]] = package
            linebuffer = ""
        else:
            linebuffer += line + "\n"
    return dict(packages)

def parse_single_build_info(packages_str: str) -> dict:
    package = None
    linebuffer = ""
    skip_lines = 0
    parser = email.parser.Parser()
    for line in packages_str.splitlines():
        if line.startswith('-----BEGIN PGP SIGNED MESSAGE-----'):
            skip_lines = 3
        if skip_lines > 0:
            skip_lines -= 1
            continue
        if line == "":
            return dict(parser.parsestr(linebuffer))
        else:
            linebuffer += line + "\n"
    if linebuffer:
        return dict(parser.parsestr(linebuffer))


    raise RuntimeError("Couldn't parse buildinfo file.")

def parse_build_depends(installed_build_depends: str) -> dict:
    """ parse the Installed-Build-Depends string of a build info file and return
    a dict with the package name as key and version as value.
    """
    depends = {}
    bp_rex = re.compile('(?P<pkgname>[0-9A-Za-z.+\-]*) \(= (?P<ver>.*)\)')

    for bp in installed_build_depends.splitlines():
        bp = bp.strip()
        bp = bp.strip(',')

        if not bp:
            continue

        match = bp_rex.match(bp)
        if not match:
            continue

        gd = match.groupdict()
        depends[gd['pkgname']] = gd['ver']

    return depends

def get_sha1(package_name: str, version: str, arch: str, session: requests.Session = None) -> tuple:
    if session is None:
        session = requests.Session()

    LOG.debug(f'sha1: Getting it for {package_name}/{version}/{arch}')

    url = f'https://{SNAPSHOT_HOST}/mr/binary/{package_name}/{version}/binfiles'
    resp = session.get(url)
    LOG.debug(f"Snapshot req {resp.request.url} took {resp.elapsed}")
    resp.raise_for_status()

    results = resp.json()['result']
    for result in results:
        if result['architecture'] in ['all', arch]:
            LOG.debug(f"sha1: Found: arch: {result['architecture']} hash: {result['hash']}")
            return (result['architecture'], result['hash'])
    return (None, None)

def download_snapshot(sha1):
    return requests.get(f'https://{SNAPSHOT_HOST}/file/${sha1}')

def download_binary(package: BinPackage, output: Path, session: requests.Session = None) -> requests.Response:
    """ download a binary """
    if session is None:
        session = requests.Session()
    session.get()

def download_snapshot_single(package: str, version: str, given_arch: str, session: requests.Session) -> tuple:
    """ download a single package from the snapshot """
    if session is None:
        session = requests.Session()

    # arch may return all if given_arch not found, but all found
    arch, sha1 = get_sha1(package, version, given_arch, session)
    if not sha1:
        errormsg = f'Failed to find a sha1 for {package}/{version}/{given_arch}.'
        LOG.warning(errormsg)
        return (False, (package, errormsg))

    resp = session.get(f'https://{SNAPSHOT_HOST}/file/{sha1}')
    LOG.debug(f"Snapshot req {resp.request.url} took {resp.elapsed}")
    resp.raise_for_status()
    return (True, (package, arch, sha1, resp.content))

def download_snapshot_pkg_ver(packages: dict, given_arch: str, output_dir: Path, session: requests.Session) -> tuple:
    """ download all listed packages/versions in dict from the snapshot server """
    # successful[name] = version
    successful_packages = {}
    failed_packages = {}

    if session is None:
        session = requests.Session()

    for name, version in packages.items():
        try:
            arch, sha1 = get_sha1(name, version, given_arch, session)
            if not sha1:
                failed_packages[name] = 'Failed to find a sha1 for {name}/{version}{given_arch}. Skipping package'
                LOG.warning(f'Failed to find a sha1 for {name}/{version}/{given_arch}. Skipping package')
                continue

            filename = f'{name}_{version}_{arch}.deb'
            pathobject = output_dir / filename

            resp = session.get(f'https://{SNAPSHOT_HOST}/file/{sha1}')
            LOG.debug(f"Snapshot req {resp.request.url} took {resp.elapsed}")
            with pathobject.open('wb') as fd:
                fd.write(resp.content)
            successful_packages[name] = filename
        except Exception as exp:
            LOG.exception(f"Failed to download package {name} {version} {arch}")
            failed_packages[name] = f"Failed to download package {name} {version} {arch}"

    return (successful_packages, failed_packages)

def download_all_build_depends(package: BinPackage, output_dir: Path, session: requests.Session) -> dict:
    """ download all build depends of a given package to path
    
        returns a tuple(dict(success), dict(failed))
    """
    build_info = get_build_info(package.source, package.source_version, package.arch, session)
    parsed_build_info = parse_single_build_info(build_info)
    build_depends = parse_build_depends(parsed_build_info['Installed-Build-Depends'])

    success, failed = download_snapshot_pkg_ver(build_depends, parsed_build_info['Architecture'], output_dir, session)
    return (success, failed)

def download_source_package(package: str):
    # package -> binary package name
    pass

def rebuild_debian_package(source_package, package, version, arch):
    pass

PKG_WITH_VERSION = re.compile(r'(?P<pkgname>[0-9A-Za-z+\-.]*) \((?P<ver>.*)\)')

def download_packages(url: str) -> dict:
    # download and parse Packages files
    # return a dict of packages. package[name] = Package
    LOG.info(f"Downloading packages from {url}")
    resp = requests.get(url)
    if url.endswith('.xz'):
        content = lzma.decompress(resp.content)
    elif url.endswith('.gz'):
        content = gzip.decompress(resp.content)
    else:
        content = resp.content

    packages = {}
    packageindex = parse_package_file(str(content, 'utf-8'))
    for name, package in packageindex.items():
        source = package['Package']
        source_version = package['Version']

        if 'Source' in package:
            source = package['Source']

        match = PKG_WITH_VERSION.match(source)
        if match:
            source, source_version = match.groups()
        
        packageobj = BinPackage(
            name=package['Package'],
            source=source,
            source_version=source_version,
            arch=package['Architecture'],
            version=package['Version'],
            filename=package['Filename'],
            sha256=package['SHA256'],
        )
        packages[package['Package']] = packageobj

    return packages

def get_filename(name, version, arch) -> str:
    """ return the correct filename """
    # e.g. 4:5.27.9-1 -> 5.27.9-1
    match = EPOCH_REX.match(version)
    if match:
        version = match.groupdict()['version']

    return f'{name}_{version}_{arch}.deb'

def test_get_filename():
    assert(get_filename('aaa', '2:1.2.2.2', 'amd64') == 'aaa_1.2.2.2_amd64.deb')

def get_debian_prefix(package: str) -> str:
    prefix = package[0]
    if package.startswith('lib'):
        prefix = package[0:4]
    return prefix

def count_flat_depends(flat_depends: dict) -> tuple:
    """ returns the amount of packages and all unique package files """
    lengths = [ len(x) for x in flat_depends.values() ]
    return (len(lengths), sum(lengths))

def convert_flat_depends(builddepends_store, architectures: list = None) -> dict:
    """ convert a build_depends.json into a flat_depends. A flat depends = { pkgname: set(versiona, versionb) }
        If architectures is None, the architectures will be ignored.
    """
    # results: one entry in the list is a {package: set(versiona, versionb)}
    flat_depends = {}
    # { pkgname: versions }
    for versions in builddepends_store.values():
        # { version: architectures }
        for v_archs in versions.values():
            # { arch: build_depends }
            for arch, bpends in v_archs.items():
                if architectures is not None and arch not in architectures:
                    continue
                for pkg, pkg_version in bpends.items():
                    if pkg not in flat_depends:
                        flat_depends[pkg] = set()
                    flat_depends[pkg].add(pkg_version)
    return flat_depends

def convert_to_tuple_depends(builddepends_store) -> dict:
    """ convert a build_depends.json into a tuple_depends. A tuple depends = { (pkgname, version, arch): list(build_depends) }
    """
    tuple_depends = {(package, version, arch):build_depends
            for package, versions in builddepends_store.items()
                for version, archs in versions.items()
                    for arch, build_depends in archs.items()}
    return tuple_depends

def convert_from_tuple_depends(tuple_depends_store) -> dict:
    """ convert a tuple_depends into a build_depends. A tuple depends = { (pkgname, version, arch): list(build_depends) }.
    """
    # TODO: find a list/dict comprehension
    build_depends = {}

    for (package, version, arch), pkg_depends in tuple_depends_store.items():
        if package not in build_depends:
            build_depends[package] = {}
        if version not in build_depends[package]:
            build_depends[package][version] = {}
        build_depends[package][version][arch] = pkg_depends

    return build_depends

TEST_BUILD_DEPENDS = {
        "coreboot-utils": {
            "4.15~dfsg-3": {
                "amd64": {
                    "autoconf": "2.71-3",
                    "bash": "5.2.15-2+b4",
                    "libpci": "5.2.15-2+b4",
                },
                "i386": {
                    "autoconf": "2.71-3",
                    "bash": "5.2.1",
                    "libpci": "5.2.15-2+b4",
                }
            }
        },
        "flashrom": {
            "1.3.0-2.1": {
                "amd64": {
                    "autoconf": "2.71-3",
                    "bash": "5.2.15-2+b4",
                    "libftdi1-2": "5.2.15-2+b4",
                },
                "i386": {
                    "autoconf": "2.71-3",
                    "bash": "5.2.15-2+b4",
                    "libftdi1-2": "5.2.15-2+b4",
                    "depsonlyi386": "86",
                }
            }
        },
}

def test_convert_flat_depends_arch_none():
    flat = convert_flat_depends(TEST_BUILD_DEPENDS, None)
    print(flat)
    assert(len(flat) == 5)
    length, sums = count_flat_depends(flat)
    assert(sums == 6)

def test_convert_flat_depends_arch_amd64():
    flat = convert_flat_depends(TEST_BUILD_DEPENDS, ["amd64"])
    print(flat)
    assert(len(flat) == 4)
    length, sums = count_flat_depends(flat)
    assert(sums == 4)

def test_convert_flat_depends_arch_i386():
    flat = convert_flat_depends(TEST_BUILD_DEPENDS, ["i386"])
    print(flat)
    assert(len(flat) == 5)
    length, sums = count_flat_depends(flat)
    assert(sums == 6)

def test_tuple_depends():
    tuple_depends = convert_to_tuple_depends(TEST_BUILD_DEPENDS)
    back = convert_from_tuple_depends(tuple_depends)
    assert(TEST_BUILD_DEPENDS == back)

def merge_build_depends(build_depends: list) -> tuple:
    """ Merge multiple build_depends give as list [tuple("filename", build_depends)].
    Returns a tuple of (merged_build_depends: dict, failures: dict).
    failures contain packages which couldn't merged.
    E.g. if 2 build_depends containing a build_depends of make, 2.4, amd64 but with different build_depends.
    """

    if not build_depends:
        return ({}, {})

    tuples_depends = [ convert_to_tuple_depends(bp) for bp in build_depends ]
    merged = {}
    for tup in tuples_depends:
        merged = merged | tup

    return convert_from_tuple_depends(merged)
