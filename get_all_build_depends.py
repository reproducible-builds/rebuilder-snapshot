#!/usr/bin/env python

import argparse
import logging
import json

import threading
import time
import queue

import pathlib

import requests
import reprobuilder

LOG = logging.getLogger('get_all_build_depends')

def get_single_build_deps(package: reprobuilder.BinPackage, session: requests.Session):
    if session is None:
        session = requests.Session()

    name = package.name
    try:
        LOG.debug('Fetching %s' % name)
        build_info = reprobuilder.get_build_info(package.source, package.version, package.arch, session)
        parsed_build_info = reprobuilder.parse_single_build_info(build_info)
        build_depends = reprobuilder.parse_build_depends(parsed_build_info['Installed-Build-Depends'])
        return (True, (name, package.version, package.arch, build_depends))
        LOG.debug('Finished %s' % name)
    except KeyboardInterrupt as exp:
        # TODO: remove this
        LOG.warning('Aborting as user requests')
        return (False, (name, package.version, package.arch, "Keyboard abort"))
    except Exception as exp:
        LOG.debug('Exception while fetching %s. %s' % (name, exp))
        return (False, (name, package.version, package.arch, "Unknown Error"))
    # except requests.exceptions.HTTPError as ex

    return (False, None, "Unknown Error")

def get_build_depends_worker(inqueue, outqueue):
    session = requests.Session()
    while True:
        task = inqueue.get()
        if task is None:
            inqueue.task_done()
            return
        result = get_single_build_deps(task, session)
        inqueue.task_done()
        outqueue.put(result)
    LOG.info('Finished worker')

def filter_packages_retry(packages, failed_build_depends):
    def _filter_packages_retry(pair):
        name, binpackage = pair
        if name not in failed_build_depends:
            return False
        if binpackage.version not in failed_build_depends[name]:
            return False
        return binpackage.arch in failed_build_depends[name][binpackage.version]

    return dict(filter(_filter_packages_retry, packages.items()))

def filter_packages_success(packages, success_build_depends):
    def _filter_packages_success(pair):
        name, binpackage = pair
        if name not in success_build_depends:
            return True
        if binpackage.version not in success_build_depends[name]:
            return True
        return binpackage.arch not in success_build_depends[name][binpackage.version]

    return dict(filter(_filter_packages_success, packages.items()))


def get_all_build_depends_thread(
        urls: dict,
        success_build_depends: dict = None,
        failed_build_depends: dict = None,
        retry_only: bool = False,
        skip_success_build: bool = True,
        num_threads: int = 4) -> tuple:

    session = requests.Session()
    repos = []
    for url in urls:
        # repo is a { 'foobar': BinPackage(foobar) }
        repo = reprobuilder.download_packages(url)
        repos.append((url, repo))

    if success_build_depends is None:
        success_build_depends = {}

    if failed_build_depends is None:
        failed_build_depends = {}

    # filter to only the packages in failed_build_depends
    if retry_only:
        repos = [ (url, filter_packages_retry(packages, failed_build_depends))
                    for url, packages in repos ]

    if skip_success_build:
        repos = [ (url, filter_packages_success(packages, success_build_depends))
                    for url, packages in repos ]

    overall = sum([ len(packages) for _, packages in repos ])
    LOG.info('Starting to fetch %d packages' % overall)
    LOG.info('Progress %d/%d/%d (suc/fail/all)' % (len(success_build_depends), len(failed_build_depends), overall))
    results = queue.Queue()
    tasks = queue.Queue()
    threads = []
    for i in range(num_threads):
        thread = threading.Thread(target=get_build_depends_worker, args=[tasks, results])
        threads.append(thread)
        thread.start()

    for url, packages in repos:
        for name, package in packages.items():
            try:
                tasks.put(package)
            except KeyboardInterrupt as exp:
                LOG.warning('Aborting as user requests')
                break

    LOG.info('Finished queueing tasks')
    # to stop workers
    for i in threads:
        tasks.put(None)


    def parse_results(resultpair):
        result, obj = resultpair
        if result:
            name, version, arch, build_depends = obj
            if name not in success_build_depends:
                success_build_depends[name] = {}
            if version not in success_build_depends:
                success_build_depends[name][version] = {}
            success_build_depends[name][version][arch] = build_depends
        else:
            name, version, arch, exception = obj
            if name not in failed_build_depends:
                failed_build_depends[name] = {}
            if version not in failed_build_depends[name]:
                failed_build_depends[name][version] = {}
            failed_build_depends[name][version][arch] = exception


    overall = sum([ len(packages) for _, packages in repos ])
    LOG.info('Progress %d/%d/%d (suc/fail/all)' % (len(success_build_depends), len(failed_build_depends), overall))
    LOG.info("Waiting for workers to clear the tasks: %d" % tasks.qsize())
    while not tasks.empty():
        LOG.info("Waiting for workers to clear the tasks: %d" % tasks.qsize())
        try:
            while not results.empty():
                try:
                    resultpair = results.get_nowait()
                    parse_results(resultpair)
                except queue.Empty:
                    pass
            time.sleep(3)
            LOG.info('Progress %d/%d/%d (suc/fail/all)' % (len(success_build_depends), len(failed_build_depends), overall))
        except KeyboardInterrupt:
            # TODO: kill
            LOG.error("User aborted")
            break

    tries = 3
    while True:
        try:
            task = results.get(timeout=3)
        except queue.Empty:
            if tries <= 0:
                break
            tries -= 1
            continue

        parse_results(task)

    LOG.info('Processed %d/%d/%d (suc/fail/all)' % (len(success_build_depends), len(failed_build_depends), overall))
    return (success_build_depends, failed_build_depends)

def get_all_build_depends(
        urls: str,
        success_build_depends: dict = None,
        failed_build_depends: dict = None,
        retry_only: bool = False,
        skip_success_build: bool = True) -> tuple:
    """ url can be 'https://ftp.debian.org/debian/dists/bullseye/main/binary-amd64/Packages.gz'
    retry_only: only package which are part of failed_build_depends will be fetched.
    skip_success_build: skip fetching build_info when already present in success_build_depends
    Single Threaded version.
    """
    session = requests.Session()
    repos = []
    for url in urls:
        # repo is a { 'foobar': BinPackage(foobar) }
        repo = reprobuilder.download_packages(url)
        repos.append((url, repo))

    if success_build_depends is None:
        success_build_depends = {}

    if failed_build_depends is None:
        failed_build_depends = {}

    # filter to only the packages in failed_build_depends
    if retry_only:
        repos = [ (url, filter_packages_retry(packages, failed_build_depends))
                    for url, packages in repos ]

    if skip_success_build:
        repos = [ (url, filter_packages_success(packages, success_build_depends))
                    for url, packages in repos ]

    overall = sum([ len(packages) for _, packages in repos ])
    LOG.info('Starting to fetch %d packages' % overall)
    for url, packages in repos:
        for name, package in packages.items():
            LOG.info('Progress %d/%d/%d (suc/fail/all)' % (len(success_build_depends), len(failed_build_depends), overall))
            try:
                LOG.info('Fetching %s' % name)
                build_info = reprobuilder.get_build_info(package.source, package.version, package.arch, session)
                parsed_build_info = reprobuilder.parse_single_build_info(build_info)
                build_depends = reprobuilder.parse_build_depends(parsed_build_info['Installed-Build-Depends'])
                if name not in success_build_depends:
                    success_build_depends[name] = {}
                if package.version not in success_build_depends[name]:
                    success_build_depends[name][package.version] = {}
                success_build_depends[name][package.version][package.arch] = build_depends

                if name in failed_build_depends:
                    if package.version in failed_build_depends[name]:
                        if package.arch in failed_build_depends[name][package.version]:
                            del failed_build_depends[name][package.version][package.arch]

                        if not failed_build_depends[name][package.version]:
                            del failed_build_depends[name][package.version]

                    # empty dict?
                    if not failed_build_depends[name]:
                        del failed_build_depends[name]

                LOG.info('Finished %s' % name)
            except KeyboardInterrupt as exp:
                LOG.warning('Aborting as user requests')
                break
            except Exception as exp:
                LOG.exception('Exception while fetching %s' % name)
                if name not in failed_build_depends:
                    failed_build_depends[name] = {}
                if package.version not in failed_build_depends[name]:
                    failed_build_depends[name][package.version] = {}
                failed_build_depends[name][package.version][package.arch] = str(exp)

    LOG.info('Processed %d/%d/%d (suc/fail/all)' % (len(success_build_depends), len(failed_build_depends), overall))
    return (success_build_depends, failed_build_depends)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--packages-url', help='an URL to a Packages or Packages.gz or Packages.xz file', required=True, action='append')
    parser.add_argument('--output', help='to save packages, defaults to build_depends.json', default='build_depends.json')
    parser.add_argument('--errors', help='to save failed packages, defaults to build_depends_failed', default='build_depends_failed.json')
    # parser.add_argument('-f', '--force', help='overwrite output/error files', action='store_true')
    parser.add_argument('--retry', help='Retry only failed packages. Errors file must exist', action='store_true')
    parser.add_argument('--skip-successful', help='Skip packages/versions which are in output file.', action='store_true')
    parser.add_argument('--verbose', help='enable verbose output', action='store_true')
    parser.add_argument('--debug', help='enable debug output', action='store_true')
    args = parser.parse_args()

    format_line = '%(asctime)s: %(name)s %(levelname)s - %(message)s'
    log_level = logging.WARNING
    if args.debug:
        log_level=logging.DEBUG
    elif args.verbose:
        log_level=logging.INFO
    logging.basicConfig(
            level=log_level,
            format=format_line
    )

    filelogger = logging.FileHandler(filename='get_all_build_depends.log')
    filelogger.setLevel(log_level)
    filelogger.setFormatter(logging.Formatter(format_line))
    logging.root.addHandler(filelogger)

    output = pathlib.Path(args.output)
    errors = pathlib.Path(args.errors)
    old_output = None
    old_errors = None

    # when retrying we have to read the errors and write it to output. output can be used for merging
    if output.exists():
        LOG.info("Output file %s exists, trying to merge file." % output)
        old_output = json.load(output.open('r'))
        LOG.info("Existing output file %s opened." % output)
    if errors.exists():
        LOG.info("Error file %s exists, trying to merge file." % errors)
        old_errors = json.load(errors.open('r'))
        LOG.info("Existing error file %s opened." % errors)

    success_build_depends, failed_build_depends = get_all_build_depends_thread(args.packages_url, old_output, old_errors, args.retry, args.skip_successful)
    LOG.info("Packing output files.")
    json.dump(success_build_depends, output.open('w'))
    json.dump(failed_build_depends, errors.open('w'))
