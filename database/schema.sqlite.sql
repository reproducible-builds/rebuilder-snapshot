-- sqlite 3 schema

create TABLE config(name TEXT PRIMARY KEY, value TEXT);
INSERT INTO config VALUES ('db_revision', 1);

create TABLE storage(id UNSIGNED BIG INT AUTO_INCREMENT UNIQUE,
		name VARCHAR(255), version VARCHAR(1024), arch VARCHAR(32),
		filename VARCHAR(255), sha1 VARCHAR(40), sha256 VARCHAR(64),
		PRIMARY KEY (name, version, arch));
create index storage_name on storage(name);
create index storage_name_version on storage(name, version);
create index storage_id on storage(id);
-- happy hash collision
create index storage_sha1 on storage(sha1);
create index storage_sha256 on storage(sha256);
create index storage_filename on storage(filename);
