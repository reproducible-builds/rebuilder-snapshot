import pathlib
import requests
import reprobuilder
import logging

logging.basicConfig(level=logging.DEBUG)

if __name__ == "__main__":
    packages = reprobuilder.download_packages('https://ftp.debian.org/debian/dists/bullseye/main/binary-amd64/Packages.gz')
    session = requests.Session()
    dest_dir = pathlib.Path('/tmp/foo')
    dest_dir.mkdir(exist_ok=True)
    g = reprobuilder.download_all_build_depends(packages['apt-utils'], dest_dir, session)

    import pprint
    pprint.pprint(g)

    import json
    json.dump(g, open('/tmp/output.json', 'w'))
