#!/bin/bash
# vim: set noexpandtab:

#
# rebuilder-snapshot - a tailored frontend to snapshot.d.o
#
# Copyright © 2023 Holger Levsen <holger@layer-acht.org>
#

set -e
set -o pipefail		# see eg http://petereisentraut.blogspot.com/2010/11/pipefail.html
trap : INT TERM

# initialize some global variables
LOOP=false
METADATA=false
PACKAGES=false
CLEANUP=false
HELP=false
DEBUG=""
VERBOSE=""

# thanks to /usr/share/doc/util-linux/examples/getopt-parse.bash
TEMP=$(getopt -o 'hdivlmpc' --long 'help,debug,ini:,verbose,loop,update-metadata,update-packages,cleanup' -n 'rebuilder-snapshot' -- "$@")
if [ $? -ne 0 ]; then
	echo 'Terminating.' >&2
	exit 1
fi
# Note the quotes around "$TEMP": they are essential!
eval set -- "$TEMP"
unset TEMP
while true; do
	case "$1" in
		'-h'|'--help')
			HELP=true
			shift
			continue
			;;
		'-d'|'--debug')
			DEBUG="--debug"
			shift
			continue
			;;
		'-i'|'--ini')
			CONFIG="$2"
			if [ ! -f "$2" ] ; then
				echo "$2 does not exist."  >&2
				exit 1
			fi
			shift 2
			continue
			;;
		'-v'|'--verbose')
			VERBOSE="--verbose"
			shift
			continue
			;;
		'-l'|'--loop')
			LOOP=true
			shift
			continue
			;;
		'-m'|'--update-metadata')
			METADATA=true
			shift
			continue
			;;
		'-p'|'--update-packages')
			PACKAGES=true
			shift
			continue
			;;
		'-c'|'--cleanup')
			CLEANUP=true
			shift
			continue
			;;
		'--')
			shift
			break
			;;
		*)
			echo 'Internal error!' >&2
			exit 1
			;;
	esac
done
# some options don't make much sense
if $LOOP && ( $METADATA || $PACKAGES || $CLEANUP ) ; then
	echo "Option --loop cannot be combined with --update-metadata nor --update-packages nor --cleanup."
	exit 1
elif ! $LOOP && ! $METADATA && ! $PACKAGES && ! $CLEANUP && ! $HELP ; then
	HELP=true
fi

# parse required config.ini file
if [ -n "$CONFIG" ] ; then
	: # set via --ini $file above
elif [ -f ./config.ini ] ; then
	CONFIG=./config.ini
elif [ -d /etc/rbuilder-snapshot/ ] && [ -f /etc/rbuilder-snapshot/config.ini ] ; then
	CONFIG=/etc/rbuilder-snapshot/config.ini
fi
if [ ! -f "$CONFIG" ] ; then
	echo "Cannot find valid config.ini file, exiting."
	exit 1
fi
BIN_DIR=$(realpath $(dirname $0))
source <(cat $CONFIG | $BIN_DIR/ini2arr.py)
DATA_DIR="${main[storage]}"
ARCHS="${main[archs]}"
SUITES="${main[suites]}"
if [ -n "${main[cache]}" ] ; then
	CACHE_URL="--cache-url ${main[cache]}"
fi
if [ -n "${main[proxy]}" ] ; then
	export http_proxy="${main[proxy]}"
	export https_proxy="${main[proxy]}"
	export ftp_proxy="${main[proxy]}"
fi
REAL_SUITES=""
for suite in $SUITES ; do
	REAL_SUITES="$REAL_SUITES $suite"
	if [ -n "${releaseextras[$suite]}" ] ; then
		for extra in ${releaseextras[$suite]} ; do
			case $extra in
				security)	REAL_SUITES="$REAL_SUITES ${suite}-security" ;;
				updates)	REAL_SUITES="$REAL_SUITES ${suite}-updates ${suite}-proposed-updates" ;;
				backports)	REAL_SUITES="$REAL_SUITES ${suite}-backports ${suite}-backports-sloppy" ;;
				*)		;;
			esac
		done
	fi
done

log_msg(){
	echo "$(date -u) - $1" | tee -a $DATA_DIR/$(basename $0).log
}

log_init(){
	log_msg "----------------------------------------"
	log_msg "Known archs:    $ARCHS"
	log_msg "Known suites:   $SUITES"
	log_msg "Verbose suites: $REAL_SUITES"
}

log_storage_stats(){
	log_init
	TODAY=$(date -u '+%Y-%m-%d')
	for arch in all $ARCHS ; do
		i=$(find $DATA_DIR/storage/files -name "*_$arch.deb" | wc -l)
		log_msg "$i $arch .deb files"
		grep -q "^$TODAY, $arch," $DATA_DIR/stats/storage.csv || echo "$TODAY, $arch, $i" >> $DATA_DIR/stats/storage.csv
	done
	i=$(find $DATA_DIR/storage/files -name "*.deb" | wc -l)
	log_msg "$i total .deb files"
	log_msg "$(du -sch $DATA_DIR/storage/files | grep -v total)"
}

help(){
	echo "$0"
	echo "		-h/--help		displays this text."
	echo "		-v/--verbose		enable verbose output."
	echo "		-d/--debug		enable debug output."
	echo "		-m/--update-metadata	updates metadata."
	echo "		-p/--update-packages	downloads packages."
	echo "		-c/--cleanup		cleanup old data and packages."
	echo "		-l/--loop		do everything in an endless loop."
}

backup_json(){
	JSON=$1
	FILE=$JSON.json
	[ -f $FILE ] || return
	TODAY=$(date -u +%Y%m%d)
	BACKUPS=$DATA_DIR/backups
	mkdir -p $BACKUPS
	[ -f $BACKUPS/$FILE.$TODAY ] || cp $DATA_DIR/$FILE $BACKUPS/$FILE.$TODAY
	# keep 14 days backup of storage.json, 1 day for the rest
	if [ "$JSON" != "storage" ] ; then
		FILE=$BACKUPS/$FILE.$(date -u -d "2 days ago" +%Y%m%d)
	else
		FILE=$BACKUPS/$FILE.$(date -u -d "14 days ago" +%Y%m%d)
	fi
	[ ! -f $FILE ] || rm $FILE
}

update_metadata(){
	log_init
	# get all build depends
	for arch in $ARCHS ; do
		log_msg "Updating metadata for $arch."
		P_OPTIONS=""
		for suite in $REAL_SUITES ; do
			if [[ $suite == *-security ]] then
				PURL="https://security.debian.org/debian-security/dists/${suite}/main/binary-${arch}/Packages.xz"
			else
				PURL="https://deb.debian.org/debian/dists/${suite}/main/binary-${arch}/Packages.xz"
			fi
			PXZ=Packages_${arch}_${suite}.xz
			[ ! -f $PXZ ] || rm $PXZ
			log_msg "Downloading $(echo $PURL |cut -d '/' -f3-)"
			if [ -n "$DEBUG" ] ; then
				curl $PURL -o $PXZ 2>&1 || exit 1
			else
				curl $PURL -o $PXZ 2> /dev/null || exit 1
			fi
			if [ "$(file -b $PXZ)" != "XZ compressed data, checksum CRC64" ] ; then
				log_msg "$PXZ is not XZ compressed, skipping."
				rm $PXZ
				continue
			fi
			if [ $(xzgrep -c ^Package: $PXZ || true) -eq 0 ] ; then
				log_msg "ignoring empty $PXZ, skipping."
				rm $PXZ
				continue
			fi
			P_OPTIONS="$P_OPTIONS --packages-url $PURL"
		done
		ALL_OPTIONS="$VERBOSE $DEBUG --skip-successful --output ${arch}_build_depends.json --errors ${arch}_build_depends_failed.json $P_OPTIONS"
		if [ -n "$DEBUG" ] ; then
			log_msg "Calling get_all_build_depends.py $ALL_OPTIONS"
		fi
		python3 $BIN_DIR/get_all_build_depends.py $ALL_OPTIONS
		backup_json ${arch}_build_depends
		backup_json ${arch}_build_depends_failed
	done
	log_msg "Updating metadata end."
	log_msg "Updating html pages."
	$BIN_DIR/make_html --ini "$CONFIG"
}

update_api(){
	log_msg "Updating sqlite for API."
	mkdir -p $DATA_DIR/database
	cp $BIN_DIR/database/schema.sqlite.sql $DATA_DIR/database/
	[ ! -f $DATA_DIR/storage.sqlite ] || mv $DATA_DIR/storage.sqlite $DATA_DIR/storage.sqlite.tmp
	python3 $BIN_DIR/storage_to_sqlite.py --database $DATA_DIR/storage.json --sqlite $DATA_DIR/storage.sqlite $VERBOSE $DEBUG
	[ ! -f $DATA_DIR/storage.sqlite.tmp ] || rm $DATA_DIR/storage.sqlite.tmp
	log_msg "Restarting the API metaservice."
	sudo systemctl restart rebuilder-snapshot-metadata.service
}

update_packages(){
	log_init
	STORAGE=$DATA_DIR/storage
	for arch in $ARCHS ; do
		log_msg "Updating packages for $arch."
		P_OPTIONS=""
		for suite in $REAL_SUITES ; do
			PXZ=Packages_${arch}_${suite}.xz
			if [ ! -f $PXZ ] && [ -n "$DEBUG" ]; then
				log_msg "ignoring nonexistent $suite/$arch."
			elif [ -s $PXZ ] ; then
				log_msg "using $suite/$arch."
				if [[ $suite == *-security ]] then
					PURL="https://security.debian.org/debian-security/dists/${suite}/main/binary-${arch}/Packages.xz"
				else
					PURL="https://deb.debian.org/debian/dists/${suite}/main/binary-${arch}/Packages.xz"
				fi
				P_OPTIONS="$P_OPTIONS --packages-url $PURL"
			elif [ -n "$DEBUG" ] ; then
				log_msg "ignoring empty $suite/$arch."
			fi
		done
		ALL_OPTIONS="$VERBOSE $DEBUG --arch $arch --builddepends ${arch}_build_depends.json --storage $STORAGE $CACHE_URL --threads 16 $P_OPTIONS"
		if [ -n "$DEBUG" ] ; then
			log_msg "Calling download_packages.py $ALL_OPTIONS"
		fi
		python3 $BIN_DIR/download_package.py $ALL_OPTIONS
		backup_json storage
		update_api
	done
	log_msg "Updating packages end."
	log_msg "Updating html pages."
	$BIN_DIR/make_html --ini "$CONFIG"
}

cleanup_lock() {
	rm -f $LOCK || true
	exit
}

#
# main
#
export LANG=C
LOCK=$DATA_DIR/$(basename $0).lock
cd $DATA_DIR
if $HELP ; then
	help
	exit 0
fi
if [ -f $LOCK ] ; then
	log_msg "$LOCK exists, exiting."
	exit 1
fi
touch $LOCK
trap cleanup_lock INT TERM
while true ; do
	START=$(date -u +%H)
	if $METADATA || $LOOP ; then update_metadata ; fi
	if $PACKAGES || $LOOP ; then update_packages ; fi
	if $CLEANUP || $LOOP ; then log_msg "Cleanup not implemented yet." ; fi
	log_storage_stats
	if ! $LOOP ; then break ; fi
	# dinstall runs 1|7|13|19:52 UTC - see https://salsa.debian.org/ftp-team/dak/blob/master/config/debian/crontab
	# so we assume dinstall has finished running at 3/9/15/21:00 UTC
	case $START in
		21|22|23|00|01|02)	NEXT_DINSTALL=3 ;;
		03|04|05|06|07|08)	NEXT_DINSTALL=9 ;;
		09|10|11|12|13|14)	NEXT_DINSTALL=15 ;;
		15|16|17|18|19|20)	NEXT_DINSTALL=21 ;;
	esac
	NOW=$(date -u +%_H)
	HOURS=$(( $NEXT_DINSTALL - $NOW ))
	if [ $HOURS -gt 0 ] && [ $HOURS -le 6 ] ; then
		HOURS=$(( $HOURS - 1 ))
		MINUTES=$(( 60 - $(date +%_M) ))
		if [ $NEXT_DINSTALL -lt 10 ] ; then NEXT_DINSTALL="0$NEXT_DINSTALL" ; fi
		log_msg "Sleeping for ${HOURS}h ${MINUTES}m now, next dinstall should be done around $NEXT_DINSTALL:00 UTC."
		log_msg "...zzzzZzzZzzzzZzzzZzzzzZZzzzzz..."
		MINUTES=$(( 60*$HOURS + $MINUTES ))
		sleep ${MINUTES}m
	else
		log_msg "No sleep, dinstall has happened in the meantime."
	fi
done
rm $LOCK
trap - INT TERM
