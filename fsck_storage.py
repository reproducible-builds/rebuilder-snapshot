#!/usr/bin/env python3
# download package to the storage

import binascii
import hashlib
import logging
import re
import argparse
import pathlib
import json

from reprobuilder import get_debian_prefix
from dataclasses import asdict, dataclass, field, replace

LOG = logging.getLogger('rebuilderstorage')

def validate_data(storage_path, name, version, _arch, filename, sha1, sha256, check_content):
    errors = []

    if check_content:
        LOG.info("Checking (also content) %s" % str((name, version, filename, sha1, sha256)))
    else:
        LOG.info("Checking %s" % str((name, version, filename, sha1, sha256)))

    prefix = get_debian_prefix(name)
    fd_file = storage_path / f'files/{prefix}/{name}' / filename
    fd_sha1 = storage_path / f'sha1/{sha1[0:2]}/{sha1[2:4]}' / sha1
    fd_sha256 = storage_path / f'sha256/{sha256[0:2]}/{sha256[2:4]}' / sha256

    if not fd_file.exists():
        errors.append(f'Missing {fd_file}.')
    if not fd_sha1.exists():
        errors.append(f'Missing {fd_sha1}.')
    if not fd_sha256.exists():
        errors.append(f'Missing {fd_sha256}.')

    if errors:
        return (False, " ".join(errors))

    if check_content:
        # check if all three files contains the same sha256
        # check if all sha1 is correct
        with open(fd_file, 'rb') as fd:
            content = fd.read()
            calc_sha256 = str(binascii.b2a_hex(hashlib.sha256(content).digest()), 'utf-8')
            if calc_sha256 != sha256:
                errors.append(f'{fd_file} contains wrong sha256 {calc_sha256}.')
        with open(fd_sha1, 'rb') as fd:
            content = fd.read()
            calc_sha1 = str(binascii.b2a_hex(hashlib.sha1(content).digest()), 'utf-8')
            if calc_sha1 != sha1:
                errors.append(f'{fd_sha1} contains wrong sha1 {calc_sha1}.')
            calc_sha256 = str(binascii.b2a_hex(hashlib.sha256(content).digest()), 'utf-8')
            if calc_sha256 != sha256:
                errors.append(f'{fd_sha1} contains the wrong data. It should contain sha256 {calc_sha256}.')
        with open(fd_sha256, 'rb') as fd:
            content = fd.read()
            calc_sha256 = str(binascii.b2a_hex(hashlib.sha256(content).digest()), 'utf-8')
            if calc_sha256 != sha256:
                errors.append(f'{fd_sha256} contains wrong sha256 {calc_sha256}.')
    if errors:
        return (False, " ".join(errors))

    return (True, None)

def clean_db_store_inval_data(storage: pathlib.Path, db_store: dict, log_store: list, check_content: bool):
    invalid_entries = []
    # add threaded version?
    for package, versions in db_store.items():
        for version, archs in versions.items():
            for arch, data in archs.items(): 
                filename, sha1, sha256 = data
                result, errors = validate_data(storage, package, version, arch, filename, sha1, sha256, check_content)
                if not result:
                    invalid_entries.append((package, version, arch, filename, sha1, sha256, errors))

    # TODO: remove invalid data
    return invalid_entries

def store_remove(db_store: dict, name: str, version: str, arch: str):
    if name in db_store:
        if version in db_store[name]:
            if arch in db_store[name][version]:
                del db_store[name][version][arch]

def remove_invalid_entries(storage_path: pathlib.Path, db_store: dict, log_store: list, errors: list) -> tuple:
    removed_entries = 0
    for name, version, arch, filename, sha1, sha256, error in errors:
        LOG.info(f"Removing entry {name}/{version}/{arch} => {filename}/{sha1}/{sha256} reason {error}")
        try:
            prefix = get_debian_prefix(name)
            fd_file = storage_path / f'files/{prefix}/{name}' / filename
            fd_sha1 = storage_path / f'sha1/{sha1[0:2]}/{sha1[2:4]}' / sha1
            fd_sha256 = storage_path / f'sha256/{sha256[0:2]}/{sha256[2:4]}' / sha256

            for fd in [fd_file, fd_sha1, fd_sha256]:
                if fd.is_file():
                    fd.unlink()
        except:
            LOG.exception("Failed to remove {name}/{version}/{arch}. Left entry in storage")
            continue

        store_remove(db_store, name, version, arch)
        log_entry = {'action': 'remove', 'package': name, 'version': version, 'arch': arch, 'filename': filename, 'sha1': sha1, 'sha256': sha256}
        log_store.append(log_entry)
        removed_entries += 1

    return db_store, log_store, removed_entries

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--storage', help='Path to the root of the filesystem store', default='./storage/')
    parser.add_argument('--database', help='Path to the storage.json', default='storage.json')
    parser.add_argument('--logbase', help='Path to the log.json', default='log.json')
    parser.add_argument('--errors', help='save failed packages', default='fsck_invalid_entries.json')
    parser.add_argument('--check-content', help='Check if the content is the same and hashs match', action='store_true')
    parser.add_argument('--remove', help='Read the errors and remove all invalid entries from the storage', action='store_true')
    parser.add_argument('--verbose', help='enable verbose output', action='store_true')
    parser.add_argument('--debug', help='enable debug output', action='store_true')
    args = parser.parse_args()

    # logging
    format_line = '%(asctime)s: %(name)s %(levelname)s - %(message)s'
    log_level = logging.WARNING
    if args.debug:
        log_level=logging.DEBUG
    elif args.verbose:
        log_level=logging.INFO
    logging.basicConfig(
            level=logging.INFO,
            format=format_line
    )

    storage = pathlib.Path(args.storage)
    database = pathlib.Path(args.database)
    logbase = pathlib.Path(args.logbase)
    errors = pathlib.Path(args.errors)

    db_store = json.load(database.open('r'))
    log_store = json.load(logbase.open('r'))

    if args.remove:
        errors_store = json.load(errors.open('r'))
        db_store, log_store, removed = remove_invalid_entries(storage, db_store, log_store, errors_store)
        LOG.info(f"Remove {removed} entries")
        json.dump(db_store, database.open('w'))
        json.dump(log_store, logbase.open('w'))
    else:
        invalid_data = clean_db_store_inval_data(storage, db_store, log_store, args.check_content)
        print("invalid data: %s" % invalid_data)
        json.dump(invalid_data, errors.open('w'))

    # TODO: generate log entries / update database
    #json.dump(db_store, database.open('w'))
    #json.dump(log_store, logbase.open('w'))
